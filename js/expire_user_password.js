/**
 * @file
 * User behaviors.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Attach handlers to evaluate the strength of any password fields and to
   * check that its confirmation is correct.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.warning_expire = {
    attach: function (context, settings) {
    	if(drupalSettings.expire_user_password!=undefined) {
    		alert(drupalSettings.expire_user_password.global.warning);
    	}
    }
  };

})(jQuery, Drupal, drupalSettings);
