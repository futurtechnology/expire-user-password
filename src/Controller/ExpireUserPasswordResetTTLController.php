<?php
/**
 * @file
 * Contains \Drupal\expire_user_password\Controller\ExpireUserPasswordResetTTLController.
 */
 
namespace Drupal\expire_user_password\Controller;
 
/**
 * ExpireUserPasswordController.
 */
class ExpireUserPasswordResetTTLController {
  /**
   * Generates an form.
   */
  public function configure() {
    
  	$form = \Drupal::formBuilder()->getForm('Drupal\expire_user_password\Form\FormResetTTL');

  	return $form;
  }      
}