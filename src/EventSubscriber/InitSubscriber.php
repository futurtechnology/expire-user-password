<?php

namespace Drupal\expire_user_password\EventSubscriber;
 
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Drupal\Component\Utility\Unicode;

use Drupal\user\Entity;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class InitSubscriber implements EventSubscriberInterface {
	
	public function checkExpiredUserPassword(GetResponseEvent $event) {

		$path_args = parse_url( $event->getRequest()->getUri() );

		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');
		
		$account = \Drupal::currentUser();
		$ttl_user = _expire_user_password_get_user_ttl($account->id());
		if (!$account->isAnonymous() && !in_array('administrator', $account->getRoles()) &&
			  $ttl_user && time() > $ttl_user && !preg_match('/^\/user\/'.$account->id().'\/(edit|password|reset)/', $path_args['path'])) {
			  	
			user_logout();
			
			drupal_set_message(t($config->get('error.expired_password_time')));
			$event->setResponse(new RedirectResponse('/user/password?warning_expired='.t($config->get('error.expired_password_time'))));
		}
	}

  /**
  * {@inheritdoc}
  */
  static function getSubscribedEvents() {
		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');
		
		$events = array();
		if ($config->get('enabled')) {
  		$events[KernelEvents::REQUEST][] = array('checkExpiredUserPassword');
		}
    return $events;
  }  
}