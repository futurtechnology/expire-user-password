<?php

/**
 * @file
* Contains \Drupal\expire_user_password\Form\FormEnableExpire.
*/

namespace Drupal\expire_user_password\Form;

use	Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Merge;

class FormResetTTL extends FormBase {

	/**
	 * {@inheritdoc}.
	 */
	public function getFormId() {
		return 'expire_user_password_reset_ttl_form';
	}

	/**
	 * {@inheritdoc}.
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');
				
		$form['#tree'] = TRUE;
		
		$form['ttl_default_global'] = array(
				'#type'        => 'fieldset',
				'#title'       => $this->t($config->get('label.default_global')),
				'#collapsible' => FALSE,
				'#collapsed'   => FALSE,
		);
		
		$ttl = $config->get('global_ttl_default') ? $config->get('global_ttl_default') : strtotime ("+6 months");
		
		$form['ttl_default_global']['ttl_global'] = array(
				'#title'         => '',
				'#type'          => 'date',
				'#description'   => $this->t($config->get('label.ttl_global_description')),
				'#default_value' => date("Y-m-d", $ttl),
		);
		
		$form['ttl_default_roles'] = array(
				'#type'        => 'fieldset',
				'#title'       => $this->t($config->get('label.ttl_default_roles')),
				'#collapsible' => FALSE,
				'#collapsed'   => FALSE,
				'#tree'        => TRUE,
		);
		
		$roles    = user_roles(TRUE);

		unset($roles['administrator']);
		unset($roles['authenticated']);
		
		foreach($roles as $key=>$oRole) {
			$roles[$key] = $oRole->get('label');
		}
		
		$form['ttl_default_roles']['role_list'] = array(
				'#title'         => $this->t($config->get('label.ttl_role_list')),
				'#type'          => 'checkboxes',
				'#description'   => $this->t($config->get('label.ttl_role_list_description')),
				'#options' 			 => $roles,
		);

		$form['save'] = array(
				'#type'   => 'submit',
				'#value'  =>$this->t('Save'),
		);
		
		return $form;		
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
		
		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');

  	if ($form_state->getValue('ttl_global') && strtotime($form_state->getValue('ttl_global')) < time()) {
    	$form_state->setError($form['ttl_default_global']['ttl_global'],$this->t($config->get('error.ttl_global_time_expired')));
  	}
	
  	$roles = $form_state->getValue('ttl_default_roles');
  	$roles = array_diff($roles['role_list'], array(0));
  	if (empty($roles)) {
    	$form_state->setError($form['ttl_default_roles']['role_list'],$this->t($config->get('error.ttl_global_role_list')));
  	}
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		set_time_limit(0);
		
		$values = $form_state->getValues();
		$roles = array_diff($values['ttl_default_roles']['role_list'], array(0));

		$query = \Drupal::entityQuery('user')
							->condition('status', 1)
							->condition('roles', $values['ttl_default_roles']['role_list'], 'IN')
							->condition('roles', 'administrator', '<>')
							->condition('uid', 0, '>');
		$uids = $query->execute();
		foreach($uids as $uid) {
			$mergeObject = new Merge(Database::getConnection(),'expire_user_password_users');
	  	$mergeObject->key('uid', $uid);
	  	$mergeObject->fields(array('time_to_live'), array(strtotime($values['ttl_default_global']['ttl_global'])));
	  	$mergeObject->execute();
		}
		
		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');

		$config->set('global_ttl_default', strtotime($values['ttl_default_global']['ttl_global']));
		$config->save();
		
		drupal_set_message($this->t($config->get('message.reset_ttl_saved')));
	}
}