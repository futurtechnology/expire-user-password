<?php
/**
 * @file
 * Contains \Drupal\expire_user_password\Controller\ExpireUserPasswordController.
 */
 
namespace Drupal\expire_user_password\Controller;
 
/**
 * ExpireUserPasswordController.
 */
class ExpireUserPasswordController {
  /**
   * Generates a form.
   */
  public function configure() {
    
  	$form = \Drupal::formBuilder()->getForm('Drupal\expire_user_password\Form\FormEnableExpire');

  	return $form;
  }      
}