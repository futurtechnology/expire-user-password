
-- SUMMARY --

The Expire user Password module allows you to update the validity period of a user's password and allows to user 
to update it if it is expired.

-- REQUIREMENTS --
  
None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/1897420 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Configure permissions to manage expire user password time

    This permission allows you to manage expire user password time..
