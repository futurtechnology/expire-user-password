<?php

/**
 * @file
* Contains \Drupal\expire_user_password\Form\FormEnableExpire.
*/

namespace Drupal\expire_user_password\Form;

use	Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Merge;

class FormEnableExpire extends FormBase {

	/**
	 * {@inheritdoc}.
	 */
	public function getFormId() {
		return 'expire_user_password_enable_form';
	}

	/**
	 * {@inheritdoc}.
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {

		$config = \Drupal::config('expire_user_password.settings');

		$form['expire_enabled'] = array(
				'#title'         => $this->t($config->get('label.expire_enabled')),
				'#type'          => 'checkbox',
				'#description'   => $this->t($config->get('label.expire_enabled_description')),
				'#default_value' => $config->get('enabled'),
		);
		
		$form['save'] = array(
				'#type'   => 'submit',
				'#value'  => t('Save'),
		);
		
		return $form;
		
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state) {

		$config = \Drupal::service('config.factory')->getEditable('expire_user_password.settings');
		
		$config->set('enabled', $form_state->getValue('expire_enabled'));
		$config->save();

		if ($form_state->getValue('expire_enabled')) {
			$timestamp = time() + $config->get('global_ttl_default');
			$query = \Drupal::entityQuery('user')
								->condition('status', 1)
								->condition('uid', 0, '>');
			$uids = $query->execute();
			foreach($uids as $uid) {
				$account = \Drupal\user\Entity\User::load($uid);
				if (!in_array('administrator', $account->getRoles())) {
					$mergeObject = new Merge(Database::getConnection(),'expire_user_password_users');
					$mergeObject->key('uid', $account->id());
					$mergeObject->fields(array('time_to_live'), array($timestamp));
					$mergeObject->execute();
				}
			}
			
			drupal_set_message(t($config->get('message.enable_expire_ok')));
		}
	}
}